if (Drupal.jsEnabled) {
  $(document).ready(function (){
    /* 
     * In drop-down mode, save rating if selection changes
     */
    $("select.karma-rating-select").change(function(){
      var uri = $(this).parents("form").find("input[@name='rating-uri']").val() + "/" + $(this).val();
      var select = $(this);
      var ratingText = $(this).parents("form").prev(".karma-rating-text");
      var ratingSaved = function (data){
        var result = Drupal.parseJson(data);
        select.next(".throbber").remove();
        ratingText.html(result['data']);
      };
      $(this).after('<span class="throbber">&nbsp;</span>');
      $.get(uri, null, ratingSaved);
    });
    
    /* 
     * In star mode, save rating if star or 0/not rated is selected
     */    
    $(".karma-stars-zero, .star-rating, .karma-not-rated").click(function(){
      var rating;
      $(this).parents("form").find("a").removeClass("karma-stars-zero-selected");
      $(this).parents("form").find("a").removeClass("karma-not-rated-selected");
      if ($(this).is(".karma-stars-zero")) {
        $(this).addClass("karma-stars-zero-selected");
        rating = 0;
      }
      else if ($(this).is(".karma-not-rated")) {
        $(this).addClass("karma-not-rated-selected");
        rating = -1;
      }
      else {
        rating = $(this).attr("class").replace("star-rating rating-", "");
      }
      var uri = $(this).parents("form").find("input[@name='rating-uri']").val() + "/" + rating;
      var select = $(this).parents("form").find("ul").next();
      var currentRating = $(this).parents("form").find("li.current-rating");
      var ratingText = $(this).parents("form").prev(".karma-rating-text");
      var ratingSaved = function (data){
        var result = Drupal.parseJson(data);
        select.next(".throbber").remove();
        currentRating.attr("style", "width: " + (parseFloat(result['current_rating']) * 17) +'px');
        ratingText.html(result['data']);
      };
      select.after('<span class="throbber">&nbsp;</span>');
      $.get(uri, null, ratingSaved);
    });
  });
}
