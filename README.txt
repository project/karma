
Karma is a comment rating and community participation system to try to keep
the comments as high-quality as possible. All users can rate all comments,
except their own, between 1 and 4. (The max can also be set to 3 or 5.)
When your comments are rated by others, those ratings are combined into a 
weighted average -- newer comments count more than older ones -- called your 
"Karma". This, roughly, represents the rating you could expect your next 
comment to receive based on your past comments. Users who have a karma 
greater than a certain minimum and who have posted a sufficient number of 
comments are considered "trusted" users, and have the added capability to rate 
comments below the normal minimum rating. (In other words, their rating scale 
is 0-4, rather than 1-4.) If enough of a user's comments are rated below 1, 
that user becomes "untrusted", which means that respected members of the 
community have repeatedly indicated that the user's input is offensive, 
content-free, or merely intended to annoy others. Said comments no longer 
appear for anonymous and regular users. Only trusted users can view them.

Many users believe that the rating system is intended to be an opportunity to
express agreement or disagreement with a post, or with the poster themself.
This is not accurate; ratings are intended to help elevate those posters that
consistently make clear, good arguments and points, regardless of content, and
to prevent trolls from invading the message board. Downrating commenters on
the basis of agreement or disagreement with their arguments leads to a
monolithic forum, free of new ideas and input.

So, please don't downrate comments just because you disagree with them!
